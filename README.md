# jACT-R 

[![pipeline status](https://gitlab.com/de.monochromata/jactr/badges/master/pipeline.svg)](https://gitlab.com/de.monochromata/jactr/commits/master)

A Java-based implementation of [ACT-R](http://act-r.psy.cmu.edu/). More info on jACT-R is available from http://www.jactr.org.
jACT-R uses [CommonReality](http://gitlab.com/de.monochromata/commonreality).
This project provides programmatic access to jACT-R models and is suitable for running jACT-R models embedded in third-party applications. Use the
[Eclipse integration of jACT-R](http://gitlab.com/de.monochromata/jactr-eclipse) to develop, debug and run jACT-R models and analyze
their results.

The Maven site is located at http://monochromata.de/maven/sites/org.jactr/

Artifacts are published to the [Maven repository]( http://monochromata.de/maven/releases/org.jactr/) and
the [p2 (Eclipse) update site](http://monochromata.de/eclipse/sites/org.jactr/).

TODO: Move all published artifacts to jactr.org and Maven central, The check on Travis CI should be based on https://github.com/amharrison/jactr or a jACT-R Team repository.